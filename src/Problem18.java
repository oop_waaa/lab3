import java.util.Scanner;
public class Problem18 {
    public static void main(String[] args) {
        int n, x;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            x = sc.nextInt();
            if (x < 5) {
                if (x == 1) {
                    System.out.print("Please input number: ");
                    n = sc.nextInt();
                    for (int i = 1; i <= n; i++) {
                        for (int j = 0; j < i; j++) {
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                } else if (x == 2) {
                    System.out.print("Please input number: ");
                    n = sc.nextInt();
                    for (int i = n; i > 0; i--) {
                        for (int j = 0; j < i; j++) {
                            System.out.print("*");

                        }
                        System.out.println();
                    }
                } else if (x == 3) {
                    System.out.print("Please input number: ");
                    n = sc.nextInt();
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < n; j++) {
                            if (j >= i) {
                                System.out.print("*");
                            } else {
                                System.out.print(" ");
                            }
                        }
                        System.out.println();
                    }
                } else if (x == 4) {
                    System.out.print("Please input number: ");
                    n = sc.nextInt();
                    for (int i = n; i >= 0; i--) {
                        for (int j = 0; j < n; j++) {
                            if (j >= i) {
                                System.out.print("*");
                            } else {
                                System.out.print(" ");
                            }
                        }
                        System.out.println();
                    }
                }
            } else if (x == 5) {
                System.out.println("Bye bye!!!");
                System.exit(0);
            } else {
                System.out.println("Error: Please input number between 1-5");
            }
        } while (x != 5);
    }
}
