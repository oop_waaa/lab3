import java.util.Scanner;
public class Problem9For {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print("Please input n: ");
        n = sc.nextInt();
        for (int x = 1; x < n + 1; x++) {
            for (int y = 1; y < n + 1; y++) {
                System.out.print(y);
            }
            System.out.println();
        }

    }
}
